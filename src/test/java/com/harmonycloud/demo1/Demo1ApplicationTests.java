package com.harmonycloud.demo1;

import org.jasypt.encryption.StringEncryptor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class Demo1ApplicationTests {

    @Autowired
    StringEncryptor stringEncryptor;

    @Test
    public void encrypt() {
        System.out.println("encrypt: " + stringEncryptor.encrypt("root"));
        System.out.println("encrypt: " + stringEncryptor.encrypt("123456"));
    }

    @Test
    public void decrypt() {
        System.out.println("decrypt: " + stringEncryptor.decrypt("dgNYLQomLbtU82vyYXHtxw=="));
    }

}
