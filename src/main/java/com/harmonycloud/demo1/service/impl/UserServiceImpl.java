package com.harmonycloud.demo1.service.impl;

import com.harmonycloud.demo1.model.entity.User;
import com.harmonycloud.demo1.mapper.UserMapper;
import com.harmonycloud.demo1.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author hongyi
 * @since 2022-09-05
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {


    @Autowired
    private UserMapper userMapper;
    public List<User> likeName(String name) {
        List<User> userList =userMapper.likeName(name);
        return userList;
    }
}
