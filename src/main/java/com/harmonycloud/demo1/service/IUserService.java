package com.harmonycloud.demo1.service;

import com.harmonycloud.demo1.model.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author hongyi
 * @since 2022-09-05
 */
public interface IUserService extends IService<User> {


    List<User> likeName(String name);
}
