package com.harmonycloud.demo1.controller;


import com.harmonycloud.demo1.model.entity.User;
import com.harmonycloud.demo1.service.IUserService;
import com.harmonycloud.demo1.service.impl.UserServiceImpl;
import com.sinopec.xnyq.common.redis.util.RedisUtil;
import com.sinopec.xnyq.common.util.R;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@AllArgsConstructor
public class TestController {


    @Autowired
    private IUserService userService;

    @GetMapping("redis")
    public R<Boolean> testRedis() {
        RedisUtil.set("test", "这是测试内容！");
        log.info(RedisUtil.get("test").toString());
        User user = userService.getById(2);
        RedisUtil.set("user", user);
        log.info(RedisUtil.get("user").toString());
        return R.success(Boolean.TRUE);
    }
}
