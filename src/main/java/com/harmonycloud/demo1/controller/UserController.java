package com.harmonycloud.demo1.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sinopec.xnyq.common.util.R;
import com.harmonycloud.demo1.model.entity.User;
import com.harmonycloud.demo1.service.IUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author hongyi
 * @since 2022-09-05
 */
@RestController
@AllArgsConstructor
@Api(value = "user", tags = {"管理模块"})
@RequestMapping(value = "users", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class UserController {

    private final IUserService userService;

    @GetMapping
    @ApiOperation("列表查询")
    public R<List<User>> getUserList(User user) {
        return R.success(userService.list(Wrappers.query(user)));
    }

    @GetMapping("page")
    @ApiOperation("分页查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "current", value = "页码", paramType = "query", dataType = "Long"),
            @ApiImplicitParam(name = "size", value = "页数", paramType = "query", dataType = "Long")
    })
    public R<Page<User>> getUserPage(Page<User> page, User user) {
        return R.success(userService.page(page, Wrappers.query(user)));
    }

    @GetMapping("{id}")
    @ApiOperation("通过id查询")
    @ApiImplicitParam(name = "id", value = "id", paramType = "path", required = true, dataType = "Long")
    public R<User> getById(@PathVariable("id") Long id) {
        return R.success(userService.getById(id));
    }

    @PostMapping
    @ApiOperation("新增")
    public R<Boolean> save(@Valid @RequestBody User user) {
        return R.success(userService.save(user));
    }

    @PutMapping
    @ApiOperation("修改")
    public R<Boolean> updateById(@Valid @RequestBody User user) {
        return R.success(userService.updateById(user));
    }

    @DeleteMapping("{id}")
    @ApiOperation("通过id删除")
    @ApiImplicitParam(name = "id", value = "id", paramType = "path", required = true, dataType = "Long")
    public R<Boolean> removeById(@PathVariable("id") Long id) {
        return R.success(userService.removeById(id));
    }

    @GetMapping("like")
    @ApiOperation("名字模糊查询")
    public R<List<User>> getLikeUser(String name){
        List<User> userList =userService.likeName(name);
        return R.success(userList);

    }


}
