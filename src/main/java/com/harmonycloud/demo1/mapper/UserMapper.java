package com.harmonycloud.demo1.mapper;

import com.harmonycloud.demo1.model.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author hongyi
 * @since 2022-09-05
 */

@Mapper
public interface UserMapper extends BaseMapper<User> {

    List<User> likeName(String name);
}
