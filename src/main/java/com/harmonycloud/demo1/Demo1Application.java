package com.harmonycloud.demo1;

import com.sinopec.xnyq.common.tenant.TenantConfig;
import com.sinopec.xnyq.common.tenant.TenantSchedule;
import com.sinopec.xnyq.common.tenant.service.impl.DataSourceServiceImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//@SpringBootApplication
@SpringBootApplication(exclude = {TenantConfig.class, TenantSchedule.class, DataSourceServiceImpl.class})
public class Demo1Application {

    public static void main(String[] args) {
        SpringApplication.run(Demo1Application.class, args);
    }

}
