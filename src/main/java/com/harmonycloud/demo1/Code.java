package com.harmonycloud.demo1;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.sinopec.xnyq.common.mybatis.generate.CodeGenerator;

public class Code {
    public static void main(String[] args) {
        CodeGenerator.generator("", "hongyi", "com.harmonycloud.demo1",
                "", new String[]{"user"},
                DbType.MYSQL, IdType.AUTO, "com.mysql.cj.jdbc.Driver", "root", "123456",
                "jdbc:mysql://127.0.0.1:3306/db01?characterEncoding=utf8&useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=GMT");
    }
}
